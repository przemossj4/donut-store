package config

import com.typesafe.config.{Config, ConfigFactory, ConfigParseOptions}
import pureconfig.error.ConfigReaderFailures

import java.io.File

trait DonutConfig {

  import pureconfig.generic.auto._

  private val configPath = sys.env.getOrElse("CONFIG_PATH", "src/main/resources/application.conf")

  private val parseOptions = ConfigParseOptions.defaults().setAllowMissing(false)

  def load(): Either[ConfigReaderFailures, (DonutStoreConfig, Config)] = {
    val config = ConfigFactory.parseFile(new File(configPath), parseOptions).resolve()

    pureconfig.loadConfig[DonutStoreConfig](config, "donut-store").map(_ -> config)
  }
}

final case class DonutStoreConfig(app: String, httpServer: HttpServer)

final case class HttpServer(ip: String, port: Int, apiVersion: String)
