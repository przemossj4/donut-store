package client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpRequest, HttpResponse, MediaTypes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.util.ByteString
import common.Donuts
import marshalling.JsonSupport

import scala.concurrent.{Await, ExecutionContextExecutor, Future}

object DonutStoreClientApp extends App with JsonSupport with SprayJsonSupport {
  import scala.concurrent.duration._
  implicit val system:       ActorSystem              = ActorSystem("akka-http-donuts-client")
  implicit val materializer: ActorMaterializer        = ActorMaterializer()
  implicit val ec:           ExecutionContextExecutor = system.dispatcher
  val restEndPoint = "http://localhost:8123"

  println("HTTP POST /app-donut")
  val jsonDonutInput = ByteString("""{"name":"plain donut", "ingredients":["sugar","flour"]}""")

  val httpPostAddDonut = HttpRequest(
    uri = s"$restEndPoint/add-donut",
    method = HttpMethods.POST,
    entity = HttpEntity(MediaTypes.`application/json`, jsonDonutInput)
  )

  println("\nHTTP GET /donuts")
  val getDonutsRequest = HttpRequest(uri = s"$restEndPoint/donuts", method = HttpMethods.GET)
  val listOfDonuts     = Await.result(sendRequest(getDonutsRequest).flatMap(Unmarshal(_).to[Donuts]), 5.second)

  println("\nHTTP POST /donuts/plain%20donut?ingredients=sugar,flour,syrup")

  val updateDonutRequest = HttpRequest(
    uri = s"$restEndPoint/donuts/plain%20donut?ingredients=sugar,flour,syrup",
    method = HttpMethods.POST
  )
  val donutUpdated = Await.result(sendRequest(updateDonutRequest).flatMap(Unmarshal(_).to[String]), 5.second)
  println(donutUpdated)

  println("\nHTTP DELETE /donuts/plain%20donut")

  val deleteDonutRequest = HttpRequest(
    uri = s"$restEndPoint/donuts/plain%20donut",
    method = HttpMethods.DELETE
  )
  val deletedDonut = Await.result(sendRequest(deleteDonutRequest).flatMap(Unmarshal(_).to[String]), 5.second)
  println(deletedDonut)

  println("Shutting down DonutStore Client Application.")
  Await.ready(system.terminate(), 5.second)

  private def sendRequest(httpRequest: HttpRequest): Future[HttpResponse] = {
    for {
      response <- Http().singleRequest(httpRequest)
      _ = println(s"${httpRequest.method} ${httpRequest.uri} status = ${response.status}")
    } yield response
  }
}
