package common

case class AppVersion(app: String, version: String)

/** A Donut item for the Donut Store application.
  *
  * @param name the name of the particular donut
  * @param ingredients the [[List]] of ingredients that make up this donut
  */
final case class Donut(name: String, ingredients: List[String])

/** A list of Donut items.
  *
  * @param donuts the root of the JSON payload with a [[Seq]] of [[Donut]] items
  */
final case class Donuts(donuts: Seq[Donut])
