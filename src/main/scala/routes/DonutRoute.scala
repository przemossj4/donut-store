package routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import common.Donut
import config.DonutStoreConfig
import data.DataApi
import marshalling.JsonSupport

import scala.util.{Failure, Success}

class DonutRoute extends HttpRoute with JsonSupport {
  override def routes()(implicit config: DonutStoreConfig, dataApi: DataApi): Route = {
    path("add-donut") {
      post {
        entity(as[Donut]) { donut =>
          onComplete(dataApi.createDonut(donut)) {
            case Success(createMessage) => complete(StatusCodes.Created, createMessage)
            case Failure(exception)     => complete(s"Failed to create donut, exception = ${exception.getMessage}")
          }
        }
      }
    } ~
      path("donuts") {
        get {
          onComplete(dataApi.fetchDonuts()) {
            case Failure(exception) => complete(s"Failed to fetch donuts, exception = ${exception.getMessage}")
            case Success(donuts)    => complete(StatusCodes.OK, donuts)
          }
        }
      } ~
      path("donuts" / Segment) { donutName =>
        post {
          parameter("ingredients") { ingredients =>
            val donutIngredients = ingredients.split(",").toList
            val donut            = Donut(donutName, donutIngredients)
            onComplete(dataApi.updateDonutIngredients(donut)) {
              case Failure(exception)     => complete(s"Failed to update ingredients, exception = ${exception.getMessage}")
              case Success(updateMessage) => complete(StatusCodes.OK, updateMessage)
            }
          }
        }
      } ~
      path("donuts" / Segment) { donutName =>
        delete {
          onComplete(dataApi.deleteDonut(donutName)) {
            case Failure(exception)     => complete(s"Failed to delete donut, exception = ${exception.getMessage}")
            case Success(deleteMessage) => complete(StatusCodes.OK, deleteMessage)
          }
        }
      } ~
      path("welcome") {
        getFromResource("welcome.html")
      } ~
      get {
        complete(StatusCodes.MethodNotAllowed)
      }
  }

}
