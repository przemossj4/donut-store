package routes

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import common.AppVersion
import config.DonutStoreConfig
import data.DataApi
import marshalling.JsonSupport

import scala.util.Try

class ApiVersionRoute extends HttpRoute with JsonSupport {

  import spray.json._

  override def routes()(implicit config: DonutStoreConfig, dataApi: DataApi): Route = {
    val apiVersion = AppVersion(app = config.app, version = config.httpServer.apiVersion)

    path("api-version") {
      get {
        parameter("prettyPrint" ? "true") { prettyPrint =>
          val shouldPrettyPrint = Try(prettyPrint.toBoolean).getOrElse(true)
          val apiVersionsOutput =
            if (shouldPrettyPrint) apiVersion.toJson.prettyPrint else apiVersion.toJson.compactPrint
          complete(HttpEntity(ContentTypes.`application/json`, apiVersionsOutput))

        }
      }
    }
  }
}
