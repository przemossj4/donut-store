package routes
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directives._
import config.DonutStoreConfig
import data.DataApi

class DefaultRoute extends HttpRoute {
  override def routes()(implicit config: DonutStoreConfig, dataApi: DataApi): Route = {
    path("") {
      getFromResource("welcome.html")
    }
  }
}
