package routes

import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import config.DonutStoreConfig
import data.DataApi

trait HttpRoute extends LazyLogging {
  logRoute()

  def routes()(implicit config: DonutStoreConfig, dataApi: DataApi): Route

  def logRoute(): Unit = {
    logger.info(s"Loading route for ${getClass.getSimpleName}")
  }

}
