package httpserver

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import config.{DonutConfig, DonutStoreConfig}
import data.{DataApi, DonutDao}
import routes.{ApiVersionRoute, DefaultRoute, DonutRoute, HttpRoute}

import scala.concurrent.ExecutionContextExecutor

trait DonutStoreServices extends LazyLogging {
  //load config
  val (donutStoreConfig, rawConfig) = new DonutConfig {}.load().fold(error => sys.error(error.toString), identity)

  logger.info(s"Loaded donut store config = $donutStoreConfig")

  //required values to be in the implicit scope
  implicit val cfg:     DonutStoreConfig = donutStoreConfig
  implicit val dataApi: DataApi          = new DonutDao()
  logger.info("Initialized data access layer")

  //Akka Http routes
  val donutApiRoutes = loadRoutes(Seq(new ApiVersionRoute(), new DonutRoute()))
  logger.info("Initialized all Akka Http routes")

  //Akka Http requires Actor System, Materializer and Execution Context
  implicit val system:       ActorSystem              = ActorSystem("donut-store-http-server")
  implicit val materializer: ActorMaterializer        = ActorMaterializer()
  implicit val ec:           ExecutionContextExecutor = system.dispatcher

  private def loadRoutes(httpRoutes: Seq[HttpRoute]): Route = {
    val defaultRoute = new DefaultRoute().routes()
    httpRoutes.foldLeft(defaultRoute)((acc, httpRoutes) => acc ~ httpRoutes.routes())
  }

}
