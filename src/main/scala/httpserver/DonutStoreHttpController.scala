package httpserver

import akka.http.scaladsl.Http
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success}

class DonutStoreHttpController extends LazyLogging {
  this: DonutStoreServices =>

  def startAndBind(): Unit = {
    logger.info("Initializing and binding Akka HTTP server")
    val httpServerFuture = Http().bindAndHandle(donutApiRoutes, cfg.httpServer.ip, cfg.httpServer.port)
    httpServerFuture.onComplete {
      case Success(binding) =>
        logger.info(s"Akka Http Server is bound to ${binding.localAddress}")
//        logger.info("To stop the server, press the [Enter] key ...")
      case Failure(exception) =>
        logger.error(s"Akka Http server failed to bind to ${cfg.httpServer.ip}:${cfg.httpServer.port}", exception)
        system.terminate()
    }

    //pressing enter key will kill the server

//    StdIn.readLine()
//    for {
//      serverBinding <- httpServerFuture
//      _             <- serverBinding.unbind()
//      terminated    <- system.terminate()
//    } yield logger.info(s"Akka Http server was terminated = $terminated")
  }

}
