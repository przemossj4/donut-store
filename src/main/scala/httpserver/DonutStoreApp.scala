package httpserver

object DonutStoreApp extends App {
  DonutStoreHttpServer.startAndBind()
}

object DonutStoreHttpServer extends DonutStoreHttpController with DonutStoreServices
