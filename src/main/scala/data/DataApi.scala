package data

import com.typesafe.scalalogging.LazyLogging
import common.{Donut, Donuts}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait DataApi {

  def createDonut(donut: Donut): Future[String]

  def fetchDonuts(): Future[Donuts]

  def updateDonutIngredients(donut: Donut): Future[String]

  def deleteDonut(donutName: String): Future[String]
}

class DonutDao extends DataApi with LazyLogging {
  import scala.concurrent.ExecutionContext.Implicits.global

  /**
    * For convenience, we are using scala.collection.concurrent.TrieMap to simulate
    * the Donut data points being stored within an external data source.
    */
  private val donutDatabase = TrieMap.empty[String, Donut]

  override def createDonut(donut: Donut): Future[String] = Future {
    logger.info(s"Creats donut = $donut")
    val donutExists = donutDatabase.putIfAbsent(donut.name, donut)
    donutExists match {
      case Some(d) => s"${d.name} already exists in database"
      case None    => s"${donut.name} has been added to the database"
    }
  }

  override def fetchDonuts(): Future[Donuts] = Future {
    logger.info("Fetching all donuts")
    Donuts(donutDatabase.values.toSeq)
  }

  override def updateDonutIngredients(donut: Donut): Future[String] = Future {
    logger.info(s"Updating ingredients = ${donut.ingredients} for donutName = ${donut.name}")
    val someDonut = donutDatabase.get(donut.name)
    someDonut match {
      case Some(d) =>
        donutDatabase.replace(d.name, donut)
        s"Updated donut ingredients for donutName = ${donut.name}"
      case None => s"Donut ${donut.name} does not exist in database. The update operation was not run."
    }
  }

  override def deleteDonut(donutName: String): Future[String] = Future {
    logger.info(s"Deleting donut = $donutName")
    val someDonut = donutDatabase.get(donutName)
    someDonut match {
      case Some(d) =>
        donutDatabase.remove(d.name)
        s"Deleted $d from database"
      case None => s"$donutName does not exist in database. The delete operation was not run."
    }
  }
}
