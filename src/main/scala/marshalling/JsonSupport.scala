package marshalling

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import common.{AppVersion, Donut, Donuts}
import spray.json._

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val apiVersionFormat: RootJsonFormat[AppVersion] = jsonFormat2(AppVersion)
  implicit val donutJsonFormat:  RootJsonFormat[Donut]      = jsonFormat2(Donut)
  implicit val donutsJsonFormat: RootJsonFormat[Donuts]     = jsonFormat1(Donuts)
}
